# Create a bash script that takes in arguments and does the installation

# log into the machine

HOSTNAME=$1
KEY=~/.ssh/nicole-key-aws.pem

# What happens if I don't give an argument (hostname)?
# Create an if condition that checks that script has been called with argument.
# If it has, set it to hostname, or else let the user know to call the script with an ip, end the script (exit 1) 

if [ -z $HOSTNAME ]
then
    read "you need to add an IP address"
    fi

ssh -o StrictHostKeyChecking=no -i $KEY ec2-user@$HOSTNAME '

sudo yum update -y

if [ sudo nginx -v];
then
    echo "Apache already installed"
else    
    sudo yum install -y httpd.x86_64
fi

sudo systemctl start httpd.service

sudo systemctl enable httpd.service

echo "Hello World" > /var/www/html/index.html


'

open http://$HOSTNAME


