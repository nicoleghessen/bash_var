## Bash script for testing Arguments and User Input

# Escape characters example

echo "'\$1"
echo "\" \" "

# Also single quotes protect double quotes and double quotes protect single quotes.

echo "I'm a cool guy"

# Argument 
# data that a function can take in and use internally 
# $ touch <argument>
# or
# $ mv <argument1> <argument2>

# in scripts if you want to use arguments they are defined with $#, where # is a number
# for example

echo $Nicole
echo $Shannon
echo $1


# if i dont like $1 it is not descriptive, i'll assign it to a variable arg1
arg1=$1
#Interpolation of variable into strings

echo "this is argument 1 $1"
echo "this is argument 1 $2"
echo "this is argument 1 $3"
echo "this is argument 1 $4" "will you break"

# To call this script with 4 arguments 