# hello_program Version 2
# make a little program that says hello to 3 people at once

# description
# the prpgram will recieve 3 name, and say hello and say hello to all 3.
# we should use good variable names: first_person, second_person, third_person




# psudo code
# I want to capture 3 arguments and reassign them to their variable names 
# I want the variable first_person to be assigned to the first argument
# I want the variable second_person to be assigned to the second argument

first_person=$1
second_person=$2


# I want to call the variable names and say hello.

echo "HELLO THERE $first_person!"
echo "HELLO THERE $second_person!"
