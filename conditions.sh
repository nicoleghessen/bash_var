# Conditions evaluate if something is results is true or false.
# This allows us to control flow - meaning our code will take different actions depending on this conditions 

# Syntax
# if ((<condition>))
# then 
   # block of code
# else 
   # block of code

# fi

# example 

COUNTER=0

if (($COUNTER < 5))
then 
  echo "counter is smaller than 5"
else
  echo "counter is larger than 5"
fi

# user input is gathered with read
echo "give me a number"
read NUMBER
if (($COUNTER < 5))
then 
  echo "counter is smaller than 5"
else
  echo "counter is larger than 5"
fi