### Bash Script, Variables and Conditions

This will be a small demo/ code along for use to learn how to use variables, arguments and conditions.

By the end we’ll have a small script that uses these to install nginx on a ubuntu machine when given an IP as an argument.

We’ll talk about the difference between arguments and variables.

We will also look into using conditions, to help us run our script - using control flow and handling errors.

#### Argument and Variable



## To cover

- Arguments
- Running script
- Variables and user input
- Conditions
- Escape notations


### Script basics 

Script is several bash commmands written in a file that a bash interprerator can execute.

This is useful to automate tasks and others.

### Running script

To run a script, it must first be executable. Check it's permissions using `ll` or `ls -l`. It should have `x`to be executed.

Add the permission "execute" by `chmod +x <file>`

You might need to do this in a remote machine as well when moving or creating new files on the fly. 

Also, running it remotely might be easier to call the file using a  full bash command.

You can run the file by just pointing to it or calling it 'bash <file>

```bash
# just pointing to a file
./bash_var.sh

# to run the file remotely, call it using bash 
bash ./bash_var.sh

# the dot just represents "HERE" you could put the entire path to the file 

/Users/nicoleghessen/code/week2/bash_variables/bash_var.sh

```

### Escape notation

In Bash and other languages, you have certain characters that behave/signal important things other than the actual symbol. For example `""` are used to outline a string of characters.

How do you print out/echo out some `"`?


**you use what is called an escape character. In bash this is the \ backlash**

```bash
# example 

echo


```

### Arguments in scripts 

Arguments are data that a function can take in and use internally 

`$ touch <argument>`
or
`$ mv <argument1> <argument2>`

In scripts if you want to use arguments they are defined with `$#`, where # is a number representing the numerical order in which the argument was passed to the script.
 
for example
```bash
echo $1
echo $1
echo $1 $2


# Interpolation of variable into strings

echo "this is argument 1 $1"
echo "this is argument 1 $2"
echo "this is argument 1 $3"
echo "this is argument 1 $4" "will you break"
```

Imagine you want to make a function, or script that takes in any number of arguments and does something?

You can use `$*` to represent all the given arguments in a list.

To explain the above we need loops!

### Loops

A loop is a block of code that run for a given number of time.
This can be useful if you have repetitive tasks.

```bash

for x in [item item 2 item3]
do 
echo "Block of code"
echo $x
done

```

In a loop, a program interates of the iterable object (usually a list), subsititung in each iteration the x for an object on the list until there are no moe objects.




### Conditions


