# loops in bash.

for x in Bacon eggs banana bread milk butter
do 
echo "you need to buy $x"
done



## Make a new list with your favourite movies, and print it out using a loop.
for movie in Titanic Kingsman Frozen "Lion King"
do
echo "My favourite movies are: $movie"
done

## Make a list of crazy_x_landlords and use a loop to print them out and at the end, tell me how many landlords were printed.
## Hint: don't just hard code - use a variable to count. Initiating a counter (Counter=0)
## Start a counter OUTSIDE the loop
## in the loop's block, increment the counter.

COUNTER=$(expr 0)
for landlord in Yann Mum Eclipse Tasha
do
echo "Crazy landlord's name: $landlord"
COUNTER=$(expr $COUNTER + 1)
done
echo $COUNTER



## Make a new list of the best restaurant and print it out using a loop in this format.
# > 1 - Sushi Samba
# > 2 - Franco Manca 
# > 3 - Anything with mushroom 

COUNTER=0
for restaurant in Alounak "Pizza Express" "Nando's" Smoque
do
COUNTER=$(expr $COUNTER + 1)
echo "$COUNTER- The best restaurants are $restaurant"
done

## Using arguments with for loops 
# $* means any number of arguments passed to script
for arg in $*
do 
echo "this was one your arguments: $arg"
done



## What if I want to count the number of items I have in the list?

